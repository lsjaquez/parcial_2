﻿namespace E1_parcial2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TB_1 = new System.Windows.Forms.TrackBar();
            this.TB_2 = new System.Windows.Forms.TrackBar();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.TB_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TB_2)).BeginInit();
            this.SuspendLayout();
            // 
            // TB_1
            // 
            this.TB_1.Location = new System.Drawing.Point(13, 13);
            this.TB_1.Name = "TB_1";
            this.TB_1.Size = new System.Drawing.Size(104, 45);
            this.TB_1.TabIndex = 0;
            this.TB_1.Scroll += new System.EventHandler(this.TB_1_Scroll);
            // 
            // TB_2
            // 
            this.TB_2.Location = new System.Drawing.Point(142, 13);
            this.TB_2.Name = "TB_2";
            this.TB_2.Size = new System.Drawing.Size(104, 45);
            this.TB_2.TabIndex = 1;
            this.TB_2.Scroll += new System.EventHandler(this.TB_1_Scroll);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(117, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TB_2);
            this.Controls.Add(this.TB_1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.TB_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TB_2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar TB_1;
        private System.Windows.Forms.TrackBar TB_2;
        private System.Windows.Forms.Label label1;
    }
}

