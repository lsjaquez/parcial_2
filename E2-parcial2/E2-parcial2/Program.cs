﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E2_parcial2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Nombre: ");
            string nombre = Console.ReadLine();
            Console.Write("Edad: ");
            int edad = int.Parse(Console.ReadLine());
            try
            {
                if (edad < 0)
                {
                    throw new MyException("Edad no valida");
                }
            }
            catch (MyException e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
