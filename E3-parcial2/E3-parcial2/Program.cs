﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace E3_parcial2
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] numeros = { 1, 5, 3,9,2 ,7,4};

            insertionSort_G.insertionSort(numeros, numeros.Length);
            for (int i = 0; i < numeros.Length; ++i)
            {
                Console.WriteLine(numeros[i]);
            }
        }
    }

    class insertionSort_G
    {
        public static void insertionSort<T>(T[] arr, int n) where T:IComparable
        {
            int i, j;
            T key;
            for (i = 1; i < n; i++)
            {
                key = arr[i];
                j = i - 1;

                /* Move elements of arr[0..i-1], that are
                   greater than key, to one position ahead
                   of their current position */
                while (j >= 0 && arr[j].CompareTo(key) > 0)
                {
                    arr[j + 1] = arr[j];
                    j = j - 1;
                }
                arr[j + 1] = key;
            }
        }
    }
}
